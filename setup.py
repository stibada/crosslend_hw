from setuptools import setup, find_packages

setup(
    name="Crosslend_hw",
    description="Crosslend Coding Challenge",
    version='1.0',
    author="Adam Stibal",
    py_modules=find_packages(),
    install_requires=[
        "pandas",
        "numpy",
        "sqlalchemy",
        "pyyaml",
        #"apache-airflow"
        ],
    python_requires='>=3.6',
    entry_points='''
        [console_scripts]
        taxi=main:main
    '''
)
