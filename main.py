import sys
from sqlalchemy import create_engine
import yaml
from datetime import date, datetime
from pipelines import (
        get_df_from_path,
        add_year_month,
        add_boroughs,
        add_rank,
        get_df_from_sql,
        save_df_to_sql,
        aggregate_by_boroughs,
        aggregate_by_passengers,
        get_diff_ranks,
        )
from cliCommandParser import (
        parser,
        parser_load,
        parser_agg_by_passengers,
        parser_agg_by_boroughs,
        parser_create_ranking,
        )


VERSION = '1.0.0'

parser_load.set_defaults(handler="_load_data")
parser_agg_by_passengers.set_defaults(handler="_aggregate_by_passengers")
parser_agg_by_boroughs.set_defaults(handler="_aggregate_by_boroughs")
parser_create_ranking.set_defaults(handler="_create_ranking")


class ArgumentsParser:
    def __init__(self, arguments):
        pargs = parser.parse_args(arguments)
        if pargs.version:
            return self._printVersion(pargs)

        if not (hasattr(pargs, "handler")):
            return parser.print_help()
        self.config = yaml.safe_load(pargs.config)
        self.lookup = get_df_from_path(self.config["lookup_data"]["path"])
        self.engine = create_engine(self.config["sql_conn"], echo=False)
        self.agg_date = date(year=2020, month=12, day=1)
        self.performAction(pargs)

    def performAction(self, pargs):
        return getattr(self, pargs.handler)(pargs)

    def _load_data(self, pargs):
        df = get_df_from_path(self.config["input_data"]["path"])
        df = add_boroughs(df, self.lookup)
        save_df_to_sql(self.engine, self.config["input_data"]["table_name"], df, self.config["input_data"]["columns_save"], if_exists='replace', index=False)

    def get_loaded_data(self):
        return get_df_from_sql(self.engine, F"SELECT * from {self.config['input_data']['table_name']}")

    # Task 2.i.
    def _aggregate_by_passengers(self, pargs):
        df = self.get_loaded_data()
        popular_destinations = aggregate_by_passengers(df, pargs.top_count)
        add_year_month(popular_destinations, self.agg_date)
        save_df_to_sql(self.engine, self.config["popular_destinations"]["table_name"], popular_destinations, self.config["popular_destinations"]["columns_save"], if_exists='replace', index=False)

    # Task 2.ii.
    def _aggregate_by_boroughs(self, pargs):
        df = self.get_loaded_data()
        popular_boroughs = aggregate_by_boroughs(df)
        add_year_month(popular_boroughs, self.agg_date)
        save_df_to_sql(self.engine, self.config["popular_boroughs"]["table_name"], popular_boroughs, self.config["popular_boroughs"]["columns_save"], if_exists='replace', index=False)

    def get_popular_boroughs(self):
        return get_df_from_sql(self.engine, F"SELECT * from {self.config['popular_boroughs']['table_name']}")

    def get_ranks(self):
        return get_df_from_sql(self.engine, F"SELECT * from {self.config['popular_rides_history']['table_name']} GROUP BY PUBorough, DOBorough")

    # Task 3 and 4
    def _create_ranking(self, pargs):
        popular_boroughs = self.get_popular_boroughs()
        popular_boroughs = add_rank(popular_boroughs, 'DOBorough_count')
        ranks_history_table = self.config['popular_rides_history']['table_name']
        if not self.engine.has_table(ranks_history_table):
            ranks_to_save = popular_boroughs
        else:
            original_ranks = self.get_ranks()
            ranks_to_save = get_diff_ranks(original_ranks, popular_boroughs)
        if not ranks_to_save.empty:
            ranks_to_save['updated'] = datetime.utcnow()
            # save history data
            save_df_to_sql(self.engine, ranks_history_table, ranks_to_save, self.config['popular_rides_history']['columns_save'], index=False, if_exists='append')
        # set current popular rides (simpler than creating a SQL view)
        save_df_to_sql(self.engine, self.config['popular_rides']['table_name'], popular_boroughs, self.config['popular_rides']['columns_save'], index=False, if_exists='replace')

    def _printVersion(self, pargs):
        print("Crosslend Coding Challenge v%s" % VERSION)

def main():
    ArgumentsParser(sys.argv[1:])


if __name__ == "__main__":
    main()
