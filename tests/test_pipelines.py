import unittest
import pandas as pd
from pipelines import add_rank

class TestPipelines(unittest.TestCase):

    def test_add_rank(self):
        df = pd.DataFrame(dict(a=[1,2,3,4,5],b=[22,11,4,8,19]))
        ranked = add_rank(df, 'b')
        correct = df.copy()
        correct['rank'] = correct['b'].rank(ascending=False, method='first')
        assert correct.equals(ranked)

