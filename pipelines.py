import pandas as pd
import numpy as np

def get_df_from_path(path, **kwargs):
    # limiting the number of rows to 10000 for demonstration purposes
    return pd.read_csv(path, nrows = 10000, **kwargs)

def save_df_to_sql(engine, name, df, columns=None, **kwargs):
    if columns is not None:
        return df.loc[:, columns].to_sql(name, con=engine, **kwargs)
    return df.to_sql(name, con=engine, **kwargs)

def get_df_from_sql(engine, query, **kwargs):
    return pd.read_sql(query, engine, **kwargs)

def add_year_month(df, date):
    df['year'] = date.year
    df['month'] = date.month

def add_boroughs(df, lookup):
    # get all possible ids for boroughs
    locations = lookup.groupby('Borough').LocationID.unique()
    # set PUBorough and DOBorough according to corresponding location ids
    conditionsPU = []
    conditionsDO = []
    values = []
    for borough, location_ids in locations.items():
        conditionsPU.append(df.PULocationID.isin(location_ids))
        conditionsDO.append(df.DOLocationID.isin(location_ids))
        # same values for both conditions
        values.append(borough)
    df['PUBorough'] = np.select(conditionsPU, values, default=np.nan)
    df['DOBorough'] = np.select(conditionsDO, values, default=np.nan)
    return df

def aggregate_by_passengers(df, k):
    # get grouped locations
    grouped = df.groupby(['PULocationID', 'DOLocationID']).apply(lambda d: d.passenger_count.sum())
    # select the top k items for every group
    return grouped.groupby('PULocationID').nlargest(k).reset_index(level=[1,2], name='passenger_sum')

def aggregate_by_boroughs(df):
    # group by PUBorough and get DOBorough counts
    result = df.groupby('PUBorough').DOBorough.value_counts().reset_index(name='DOBorough_count')
    return result

def add_rank(df, column):
    df['rank'] = df[column].rank(ascending=False, method='first')
    return df

def get_diff_ranks(first_df, last_df):
    # remove duplicates in terms of all 3 columns
    deduplicated = pd.concat([first_df, last_df]).drop_duplicates(subset=['PUBorough', 'DOBorough', 'rank'], keep=False)
    # keep only the new entries
    return deduplicated.drop_duplicates(subset=['PUBorough', 'DOBorough'], keep='last')
