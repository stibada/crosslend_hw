
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.docker_operator import DockerOperator

default_args = {
'owner'                 : 'airflow',
'description'           : 'Sample Taxi DAG with DockerOperator',
'depend_on_past'        : False,
'start_date'            : datetime(2021, 7, 21),
'email_on_failure'      : False,
'email_on_retry'        : False,
'retries'               : 1,
'retry_delay'           : timedelta(minutes=5)
}

# run once a month
with DAG('crosslend_taxi_hw', default_args=default_args, schedule_interval="0 0 1 * *", catchup=False) as dag:
    task_load_data = DockerOperator(
        task_id='docker_load_data',
        image='docker_image_crosslend',
        container_name='task___command_load_data',
        api_version='auto',
        auto_remove=True,
        command="taxi load",
        docker_url="unix://var/run/docker.sock",
        network_mode="bridge"
        )

    task_aggregate_by_passengers = DockerOperator(
        task_id='docker_aggregate_by_passengers',
        image='docker_image_crosslend',
        container_name='task___command_aggregate_by_passengers',
        api_version='auto',
        auto_remove=True,
        command="taxi aggregate_by_passengers",
        docker_url="unix://var/run/docker.sock",
        network_mode="bridge"
        )

    task_aggregate_by_boroughs = DockerOperator(
        task_id='docker_aggregate_by_boroughs',
        image='docker_image_crosslend',
        container_name='task___command_aggregate_by_boroughs',
        api_version='auto',
        auto_remove=True,
        command="taxi aggregate_by_boroughs",
        docker_url="unix://var/run/docker.sock",
        network_mode="bridge"
        )

    task_create_ranking = DockerOperator(
        task_id='docker_create_ranking',
        image='docker_image_crosslend',
        container_name='task___command_create_ranking',
        api_version='auto',
        auto_remove=True,
        command="taxi create_ranking",
        docker_url="unix://var/run/docker.sock",
        network_mode="bridge"
        )

    task_load_data >> [task_aggregate_by_passengers, task_aggregate_by_boroughs]
    task_aggregate_by_boroughs >> task_create_ranking
