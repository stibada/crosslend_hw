import argparse

parser = argparse.ArgumentParser(description="(2021)\nCrosslend Coding Challenge", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("--version", help="print version info", action='store_true', dest="version", default=False)
parser.add_argument("-c", "--config", help="configuration file", type=argparse.FileType('r'), dest="config", default="./config.yml")
subparsers = parser.add_subparsers(title="commands")

# get data command
parser_load = subparsers.add_parser("load", help="load new data")
parser_load.add_argument("-f", "--file", help="file with data", type=argparse.FileType('r'), dest="file", default="./data/yellow_tripdata_2020-12.csv")

# aggregate by passengers
parser_agg_by_passengers = subparsers.add_parser("aggregate_by_passengers", help="get popular locations in terms of passengers")
parser_agg_by_passengers.add_argument("-k", help="select top k destinations", type=int, dest="top_count", default=3)

# aggregate by boroughs
parser_agg_by_boroughs = subparsers.add_parser("aggregate_by_boroughs", help="get popular boroughs in terms of rides count")

# set rankings
parser_create_ranking = subparsers.add_parser("create_rankings", help="create rankings for boroughs")
