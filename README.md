# Crosslend Homework
Crosslend homework with Taxi Data

# install
clone the repository and cd
```bash
git clone https://gitlab.com/stibada/crosslend_hw.git
cd crosslend_hw
```
create virtualenv or install the package globally
```bash
python3 -m venv venv
source venv/bin/activate
pip install --editable .
```
# description
This is the solution to the Crosslend Homework for Data Engineers. There were made a few assumptions to create shortcuts in order to be able to simulate a real usage of the pipelines.
1. There is only one input file with yellow taxi rides from December 2020
1. Only the first 10000 rides are considered to keep the data file size low
1. Lookup table is loaded from csv, for the demonstration, there is no need to keep it in the database
1. database used for the solution is sqlite3, because there is no need to run a separate DB server. The only difference in real use would be another connection string in config.py (or as a command line argument) using e.g. Postgresql.
1. all the tasks shall be possible to execute separately so that they can be bound into Apache Airflow into separate tasks
1. Apache Airflow example DAG is a demonstration how would such a DAG look like, if the solution were dockerized
# usage
The solution is packed as a python package with several commands:
```bash
$ taxi -h
usage: taxi [-h] [--version] [-c CONFIG]
            {load,aggregate_by_passengers,aggregate_by_boroughs,create_rankings}
            ...

(2021)
Crosslend Coding Challenge

optional arguments:
  -h, --help            show this help message and exit
  --version             print version info
  -c CONFIG, --config CONFIG
                        configuration file

commands:
  {load,aggregate_by_passengers,aggregate_by_boroughs,create_rankings}
    load                load new data
    aggregate_by_passengers
                        get popular locations in terms of passengers
    aggregate_by_boroughs
                        get popular boroughs in terms of rides count
    create_rankings     create rankings for boroughs

```
The order of the correct execution is captured in the sample_dag.py file:
```python
task_load_data >> [task_aggregate_by_passengers, task_aggregate_by_boroughs]
task_aggregate_by_boroughs >> task_create_ranking
```
The data will be loaded from file and saved to the database so that other tasks can load it later. Using Airflow XComs would not be recommended for such large datasets (more than 100 MB).
```bash
taxi load
```
The second step is to aggregate the data and create rankings according to the assignment
```bash
taxi aggregate_by_passengers
taxi aggregate_by_boroughs
taxi create_rankings
```
This will create or update the corresponding tables in the database, using the options from the configuration file config.yml.

# examples
Get the three most common destination in December 2020:
```sql
SELECT DOLocationID, passenger_sum FROM popular_destinations WHERE year=2020 AND month=12 ORDER BY passenger_sum DESC LIMIT 3;
```
Get the second most rides starting in Queens end in December 2020?
```sql
SELECT DOBorough FROM popular_boroughs WHERE year = 2020 AND month = 12 AND PUBorough = 'Queens' ORDER BY DOBorough_count DESC LIMIT 1 OFFSET 1;
```

# further development
The functionality of the solution is pretty limited. There is also a limited usage of the aggregated data, as the informations about trip length, trip duration, tip amount and others are not configured to be saved. 
### data checking
There is no validity checking of the data, as the source is for this demonstration trusted. In the production environment should have been the input data well checked for the right datetime, empty values, non-existing locations and so on.
### automated data loading
The demonstration loads the data from a file, but in real world, there should be another way considered. In the best case loading from a corresponding API.
### Airflow deployment
The Airflow DAG has not been tested as it serves as an illustration, how the DAG using a Docker image with the package installed, would be configured.
### enrich history tables
If the history table should contain some additional information about longest and shortest trip, avg. tip amount and so on, it would be necessary to enrich the original grouped dataframe in the function pipelines.aggregate_by_boroughs, as the ranking uses the very table to create the actual ranking. There should be a discussion about the right architecture for that case.
### another aggregation windows
To create daily or yearly aggregations instead of monthly, the database schema would have to change by adding/removing appropriate day/month/year columns. Additionally the input data would have to be iterated in the appropriate way. Increasing the aggregation window should be still possible (although with limited usage) using the SQL aggregate functions on the original tables.
### testing
There is a single test suite TestPipelines with a single test called test_add_rank, which is a very simple test just for demonstration purposes. In the real world, the test coverage should be as close to 100% as possible. Running the tests from the repo directory:
```python
python3 -m unittest tests/test*.py
```
The test coverage can be distilled using the coverage.py package:
```bash
coverage run -m unittest tests/test_*.py
coverage report -m
```
